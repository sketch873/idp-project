#!/bin/bash

docker build -t client_idp ./client
docker build -t admin_idp ./admin
docker build -t server_idp ./server

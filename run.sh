#!/bin/bash

if [ "$#" -ne 1 ]
then
	echo "./run.sh <image>"
else
	docker run -p 8080:8080 $1
fi	

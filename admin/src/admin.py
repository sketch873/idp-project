from sanic import Sanic
from sanic import response
import requests as req
import json

app = Sanic()

categories = ["Human resources", "Payment methods", "Application error"]

def print_log_from_json(endpoint, json_msg):
    print('Message on /' + endpoint)
    for i in json_msg.keys():
        print('\t' + i + ' : ' + json_msg[i])

def print_log_err(endpoint, err):
    print('Error from /' + endpoint)
    print('\t' + err);

@app.route("/unresolvedTicketsCategory")
async def unresolved_tickets_category(request):
    json_obj = request.json

    print_log_from_json("unresolvedTicketsCategory", json_obj)    
    if "category" not in json_obj.keys():
        print_log_err('unresolvedTicketsCategory', 'No category')
        return response.json({'SolvingTicket': False})
    elif json_obj["category"] not in categories:
        print_log_err('unresolvedTicketsCategory', 'No such category')
        return response.json({'SolvingTicket': False})

    res = req.post(url="http://server.idp.ro:8080/unresolvedTicketsCategory", json=request.json);
    
    res_json = json.loads(res.text)
    dicts = []
    for i in range(len(res_json["ticket_ids"])):
        ticket = {"ticket_id" : res_json["ticket_ids"][i]}
        ticket["subject"] = res_json["subjects"][i]
        dicts.append(ticket)

    return response.json({"Response" : str(dicts)}) 

@app.route("/pullOneTicket")
async def pull_one_ticket(request):
    json_obj = request.json

    print_log_from_json('pullOneTicket', json_obj)    
    if "ticket_id" not in json_obj.keys():
        print_log_err('pullOneTicket', 'No ticket ID')
        return response.json({'PullingTicket': False})

    res = req.post(url="http://server.idp.ro:8080/pullOneTicket", json=request.json);

    return response.json({"Response" : res.text})

@app.route("/solveTicket")
async def solve_ticket(request):
    json_obj = request.json

    print_log_from_json("solveTicket", json_obj)    
    if "ticket_id" not in json_obj.keys():
        print_log_err('solveTicket', 'No ticket ID!')
        return response.json({'SolvingTicket': False})
    elif "ticket_response" not in json_obj.keys():
        print_log_err('solveTicket', 'No category!')
        return response.json({'SolvingTicket': False})
    elif len(json_obj['ticket_response']) >= 3000:
        print_log_err('solveTicket', 'Message too long!')
        return response.json({'SolvingTicket': False})

    res = req.post(url="http://server.idp.ro:8080/solveTicket", json=request.json);
    return response.json({"Response" : res.text}) 

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8082)

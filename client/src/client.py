from sanic import Sanic
from sanic import response
import requests as req
import json

app = Sanic()

categories = ["Human resources", "Payment methods", "Application error"]

def print_log_from_json(endpoint, json_msg):
    print('Message on /' + endpoint)
    for i in json_msg.keys():
        print('\t' + i + ' : ' + json_msg[i])

def print_log_err(endpoint, err):
    print('Error from /' + endpoint)
    print('\t' + err);

@app.route("/writeTicket")
async def write_ticket(request):
    json_obj = request.json

    print_log_from_json("writeTicket", json_obj)    
    if "ticket_message" not in json_obj.keys():
        print_log_err('writeTicket', 'No message!')
        return response.json({'WritingTicket': False})
    elif "category" not in json_obj.keys():
        print_log_err('writeTicket', 'No category!')
        return response.json({'WritingTicket': False})
    elif "subject" not in json_obj.keys():
        print_log_err('writeTicket', 'No subject!')
        return response.json({'WritingTicket': False})
    elif len(json_obj['ticket_message']) >= 3000:
        print_log_err('writeTicket', 'Message too long!')
        return response.json({'WritingTicket': False})
    elif json_obj['category'] not in categories:
        print_lor_err('writeTicket', 'Not a category')
        return response.json({'WritingTicket': False})

    res = req.post(url="http://server.idp.ro:8080/writeTicket", json=request.json);
    return response.json({"Response" : res.text}) 

@app.route("/checkResponse")
async def check_response(request):
    json_obj = request.json

    print_log_from_json("checkResponse", json_obj)
    if 'ticket_id' not in json_obj.keys():
        print_log_err('checkResponse', 'No ticket id')
    
    res = req.post(url="http://server.idp.ro:8080/checkResponse", json=request.json);
    print("RESPONSE : " + res.text)
    return response.json({"Response" : res.text}) 

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8081)

var mysql = require('mysql2');
var express = require ('express');
var app = express();

var con = mysql.createConnection({
	host: "database.idp.ro",
	port: 3306,
	user: "root",
	password: "banana",
	database: "ticketdb"
});


con.connect(function(err) {
	console.log("Connected!");
	if (err) throw err;

});

const FALSE = 0;
const TRUE = 1;

/*  Generate tables if they are missing  */
con.query("CREATE TABLE tickets (ticket_id VARCHAR(65) PRIMARY KEY, subject VARCHAR(100), category VARCHAR(100), ticket_message VARCHAR(3000), ticket_response VARCHAR(3000), resolved BOOLEAN);", function (err, result, field) {
});

con.query("CREATE TABLE metrics (type VARCHAR(50), number INT, category VARCHAR(100), PRIMARY KEY(type, category));", function(err, result, fields) {
});

con.query('INSERT INTO metrics (type, number, category) VALUES ("queued", 0, "Human resources");', function(err, result, fields){})
con.query('INSERT INTO metrics (type, number, category) VALUES ("resolved", 0, "Human resources");', function(err, result, fields){})
con.query('INSERT INTO metrics (type, number, category) VALUES ("queued", 0, "Payment methods");', function(err, result, fields){})
con.query('INSERT INTO metrics (type, number, category) VALUES ("resolved", 0, "Payment methods");', function(err, result, fields){})
con.query('INSERT INTO metrics (type, number, category) VALUES ("queued", 0, "Application error");', function(err, result, fields){})
con.query('INSERT INTO metrics (type, number, category) VALUES ("resolved", 0, "Application error");', function(err, result, fields){})

/*  Generates a random string ID. */
function generator() {
	const symbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	var result = ''
	for (var i = 0; i < 64; i++)
		result += symbols[parseInt(Math.random() * symbols.length)];

	return result;
}

app.use(express.json());

app.post('/writeTicket', function(req, res) {
	var ticket_id = generator();
	console.log("New write")
	console.log("\t" + ticket_id);
	console.log("\t" + req.body.subject);
	console.log("\t" + req.body.category);
	console.log("\t" + req.body.ticket_message);
	var str = 'INSERT INTO tickets (ticket_id, subject, category, ticket_message, resolved) VALUES ("' + ticket_id + '","' + req.body.subject + '","' + req.body.category + '","' + req.body.ticket_message + '","' + FALSE + '");';
	con.query(str, function (err, result, fields) {
		console.log("ERROR")
		console.log(err)
		if (err) res.send("FAIL")
		else res.send(ticket_id)
 	});
	
	/*  Update statistics  */
	str = 'UPDATE metrics SET number = number + 1 WHERE type = "queued" AND category = "' + req.body.category + '";';
	con.query(str, function (err, result, fields) {
 	});

});

app.post('/checkResponse', function(req, res) {
	var str = 'SELECT ticket_response, resolved FROM tickets WHERE ticket_id="' + req.body.ticket_id + '";';
	con.query(str, function (err, result, fields) {
		Object.keys(result).forEach(function(key) {
      			var row = result[key];
			if (row && row.ticket_response) {
      				res.send(row.ticket_response)
			} else
				res.send("Not yet")
    			});
		
 	});
});

app.post('/solveTicket', function(req, res) {
	var str = 'UPDATE tickets SET ticket_response = "' + req.body.ticket_response + '", resolved = ' + TRUE + ' WHERE ticket_id = "' + req.body.ticket_id + '";';
	con.query(str, function (err, result, fields) {
 		if(err)
			res.send("Failed!")
		else
			res.send("Done!")
	});
	
	str = 'UPDATE metrics SET number = number + 1 WHERE type = "resolved" AND category = "' + req.body.category + '";';
	con.query(str, function (err, result, fields) {
	});
});

app.post('/unresolvedTicketsCategory', function(req, res) {
	var str = 'SELECT ticket_id, subject FROM tickets WHERE resolved = ' + FALSE + ' AND category = "' + req.body.category + '";';
	con.query(str, function (err, result, fields) {
		if (result) {
			var ticket_ids = []
			var subjects = []
			Object.keys(result).forEach(function(key) {
      				var row = result[key];
				if (row) {
					ticket_ids.push(row.ticket_id)
					subjects.push(row.subject)
				}
    				});
			var dict = { "ticket_ids" : ticket_ids, "subjects" : subjects}
			console.log(dict)
			res.send(dict)
		} else {
			res.send("UNRESOLVED TICKET CATEGORY")
		}
 	});

});

app.post('/pullOneTicket', function(req, res) {
	var str = 'SELECT ticket_message FROM tickets WHERE ticket_id = "' + req.body.ticket_id + '";';
	con.query(str, function (err, result, fields) {
		if (result) {
			Object.keys(result).forEach(function(key) {
      				var row = result[key];
				if (row) {
					res.send(row.ticket_message)
				}
    				});
		} else {
			res.send("UNRESOLVED PULLING TICKET")
		}
 	});

});

var server = app.listen(8080, function() {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Server side listening at http://%s:%s", host, port)
});

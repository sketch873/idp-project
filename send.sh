#!/bin/bash

if [ "$#" -ne 2 ]
then
	echo "./send.sh <json> <endpoint>"
else
	curl --request GET --url 172.0.0.1:8081/$2 --header 'content-type: application/json' -d @$1
fi	

